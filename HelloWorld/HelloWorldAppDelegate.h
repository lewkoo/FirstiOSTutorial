//
//  HelloWorldAppDelegate.h
//  HelloWorld
//
//  Created by Levko Ivanchuk on 2013-06-23.
//  Copyright (c) 2013 Levko Ivanchuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelloWorldAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
