//
//  HelloWorldViewController.h
//  HelloWorld
//
//  Created by Levko Ivanchuk on 2013-06-23.
//  Copyright (c) 2013 Levko Ivanchuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelloWorldViewController : UIViewController <UITextFieldDelegate>

@property (copy,nonatomic) NSString *userName;


@end
